/***************************************************************************
 *  gdpd.cpp
 *  Part of GdPd
 *  2023- see README for AUTHORS
 *  https://gitlab.univ-lille.fr/ivmi
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 130, Boston, MA 02111-1307, USA.
 */

#include "gdpd.h"


#include <godot_cpp/core/class_db.hpp>
#include <godot_cpp/classes/os.hpp>
#include <godot_cpp/classes/global_constants.hpp>
#include <godot_cpp/classes/label.hpp>
#include <godot_cpp/variant/utility_functions.hpp>
#include <godot_cpp/classes/audio_server.hpp>

using namespace godot;

const int PCM_BUFFER_SIZE = 4096 * 4;

void GdPd::_bind_methods() {
	ADD_GROUP("GdPd", "gdpd_");

	ClassDB::bind_method(D_METHOD("get_available_input_devices"),
					&GdPd::get_available_input_devices);
    ClassDB::bind_method(D_METHOD("get_available_output_devices"),
					&GdPd::get_available_output_devices);
    ClassDB::bind_method(D_METHOD("init_devices"), &GdPd::init_devices);
    ClassDB::bind_method(D_METHOD("init_parameters"), &GdPd::init_parameters);
    ClassDB::bind_method(D_METHOD("init_nort"), &GdPd::init_nort);
    ClassDB::bind_method(D_METHOD("stop"), &GdPd::stop);
    ClassDB::bind_method(D_METHOD("openfile"), &GdPd::openfile);
    ClassDB::bind_method(D_METHOD("closefile"), &GdPd::closefile);
    ClassDB::bind_method(D_METHOD("subscribe"), &GdPd::subscribe);
    ClassDB::bind_method(D_METHOD("has_message"), &GdPd::has_message);
    ClassDB::bind_method(D_METHOD("get_next"), &GdPd::get_next);
    ClassDB::bind_method(D_METHOD("start_message"), &GdPd::start_message);
    ClassDB::bind_method(D_METHOD("add_symbol"), &GdPd::add_symbol);
    ClassDB::bind_method(D_METHOD("add_float"), &GdPd::add_float);
    ClassDB::bind_method(D_METHOD("finish_list"), &GdPd::finish_list);
    ClassDB::bind_method(D_METHOD("set_volume"), &GdPd::set_volume);

}

GdPd::GdPd() : m_vol(1) {
	//create message array
	m_messages = new Array();
	m_init=false;
}

GdPd::~GdPd() {}



int GdPd::audioCallback(void *outputBuffer, void *inputBuffer, 
					    unsigned int nBufferFrames, double streamTime, 
						RtAudioStreamStatus status, void *userData){
	GdPd* gdpd = static_cast<GdPd*>(userData);
	gdpd->processAudio(outputBuffer, inputBuffer, nBufferFrames);
	return 0;
}


Array GdPd::get_available_input_devices() {
	Array gdlist;
	for(int d=0; d<m_audio.getDeviceCount(); d++) {
		if(m_audio.getDeviceInfo(d).inputChannels>0) {
			gdlist.push_back(m_audio.getDeviceInfo(d).name.c_str());
		}
	}
	return gdlist;
}

Array GdPd::get_available_output_devices() {
	Array gdlist;
	for(int d=0; d<m_audio.getDeviceCount(); d++) {
		if(m_audio.getDeviceInfo(d).outputChannels>0) {
			gdlist.push_back(m_audio.getDeviceInfo(d).name.c_str());
		}
	}
	return gdlist;
}


int GdPd::init_devices(String inputDevice, String outputDevice) {
	std::string inpStr(inputDevice.utf8().get_data());
	std::string outStr(outputDevice.utf8().get_data());

	for(int d=0; d<m_audio.getDeviceCount(); d++) {
		std::string n = m_audio.getDeviceInfo(d).name;
		if(n.compare(inpStr)==0) {
			m_inputDevice = d;
		}
		if(n.compare(outStr)==0) {
			m_outputDevice = d;
		}
	}

	RtAudio::DeviceInfo inpInfo = m_audio.getDeviceInfo(m_inputDevice);
	RtAudio::DeviceInfo outInfo = m_audio.getDeviceInfo(m_outputDevice);
	m_nbInputs = inpInfo.inputChannels;
	m_nbOutputs = outInfo.outputChannels;
	m_sampleRate = outInfo.preferredSampleRate;
	m_bufferFrames = 128;

    m_realtime=true;

	return start();
}

int GdPd::init_parameters(int nbInputs, int nbOutputs, int sampleRate, int bufferSize) {
	m_inputDevice = m_audio.getDefaultInputDevice();
	m_outputDevice = m_audio.getDefaultOutputDevice();
	RtAudio::DeviceInfo inpInfo = m_audio.getDeviceInfo(m_inputDevice);
	RtAudio::DeviceInfo outInfo = m_audio.getDeviceInfo(m_outputDevice);
	m_nbInputs 	= std::min<int>(nbInputs, inpInfo.inputChannels);
	m_nbOutputs = std::min<int>(nbOutputs, outInfo.outputChannels);
	m_sampleRate = sampleRate;
	m_bufferFrames = std::max<int>(64, bufferSize);

    m_realtime=true;

	return start();
}

int GdPd::init_nort() {

    m_nbInputs=0;
    m_nbOutputs=2;
    m_sampleRate=44100;
    m_realtime=false;


	if(!m_pd.init(m_nbInputs, m_nbOutputs, m_sampleRate, true)) {
		print("GDPD : Error starting libpd");
		return 1;
	}

	// Create message hook
	m_pd.subscribe("to_gdpd");
	m_pd.setReceiver(this);
	m_init=true;

    // Create stream player
	AudioStreamPlayer* p = memnew(AudioStreamPlayer());
    add_child(p);

    Ref<GdPdStream> s = memnew(GdPdStream());
    s->setGdPd(this);

    p->set_stream(s);
    p->play();

	print("Initialized");

	return 0;
}

int GdPd::start() {
    RtAudio::StreamParameters outParams, inpParams;
    inpParams.deviceId = m_inputDevice;
    inpParams.nChannels = m_nbInputs;
    outParams.deviceId = m_outputDevice;
    outParams.nChannels = m_nbOutputs;
    print("Output channels = "+std::to_string(outParams.nChannels));
    print("Input channels = "+std::to_string(inpParams.nChannels));


	if(!m_pd.init(m_nbInputs, m_nbOutputs, m_sampleRate, true)) {
		print("GDPD : Error starting libpd");
		return 1;
	}

	//libpd_set_verbose(1);

	//start dsp
	m_pd.computeAudio(true);

    if(m_realtime) {
        //intialize rtaudio
        if(m_audio.getDeviceCount()==0){
            UtilityFunctions::print("There are no available sound devices.");
            return 1;
        }

        RtAudio::StreamOptions options;
        options.streamName = "gdpd";
        options.flags = RTAUDIO_SCHEDULE_REALTIME;
        if(m_audio.getCurrentApi() != RtAudio::MACOSX_CORE) {
            options.flags |= RTAUDIO_MINIMIZE_LATENCY;
        }
        try {
            m_audio.openStream(&outParams, &inpParams, RTAUDIO_FLOAT32, 
                               m_sampleRate, &m_bufferFrames, &audioCallback, 
                               this, &options);
            m_audio.startStream();
            print("Stream started");
        }
        catch(RtAudioError& e) {
            UtilityFunctions::print(e.getMessage().c_str());
        }
    }
    else {
    }

	//create message hook
	m_pd.subscribe("to_gdpd");
	m_pd.setReceiver(this);
	m_init=true;

	print("Initialized");

	return 0;
}

void GdPd::stop() {
	m_init=false;
    if(m_realtime) {
        m_audio.stopStream();
        m_audio.closeStream();
    }
    else {
    }
	m_pd.computeAudio(false);
	print("Stopped");
}

void GdPd::processAudio(void *outputBuffer, void *inputBuffer, 
						unsigned int nBufferFrames) {
	int ticks = nBufferFrames / libpd_blocksize();

	m_pd.processFloat(ticks, (float*)inputBuffer, (float*)outputBuffer);

	//volume control on the output
	for(int b=0; b<nBufferFrames*m_nbOutputs; ++b) {
		((float*)outputBuffer)[b]*=m_vol;
	}
}

bool GdPd::openfile(godot::String baseStr, godot::String dirStr) {
	std::string baseS(baseStr.utf8().get_data());
	std::string dirS(dirStr.utf8().get_data());

	if(m_patchsMap.find(baseS)!=m_patchsMap.end()) {
		print("Patch "+baseS+" already opened");
		return true;
	}

	pd::Patch p1 = m_pd.openPatch(baseS.c_str(), dirS.c_str());
	if(!p1.isValid()) {
		print("Could not open patch "+baseS);
        return false;
	}
	else {
		print("Opened patch "+baseS);
		m_patchsMap[baseS] = p1;
	}
    return true;
}

void GdPd::closefile(godot::String baseStr) {
	std::string baseS(baseStr.utf8().get_data());
	if(m_patchsMap.find(baseS)!=m_patchsMap.end()) {
		m_pd.closePatch(m_patchsMap[baseS]);
		m_patchsMap.erase(baseS);
		print("Closed patch "+baseS);
	}
}

void GdPd::subscribe(String symbStr) {
	std::string symbS(symbStr.utf8().get_data());
	m_pd.subscribe(symbS.c_str());
}

bool GdPd::has_message() {
	if(m_init) {
		//receive new messages
		m_pd.receiveMessages();
	}

	//return if more than one message
	int size = m_messages->size();
    return size>0;
}

Array GdPd::get_next() {
	Array msg = m_messages->pop_front();
	return msg;
}

int GdPd::blocksize() {
	int blocksize = libpd_blocksize();
	return blocksize;
}

int GdPd::start_message(int nbValues) {
    int res = libpd_start_message(nbValues);
	return res;
}

void GdPd::add_symbol(String symbStr) {
	std::string symbS(symbStr.utf8().get_data());
	libpd_add_symbol(symbS.c_str());
}

void GdPd::add_float(float val) {
	libpd_add_float(val);
}

int GdPd::finish_list(String destStr) {
	std::string destS(destStr.utf8().get_data());
    int res = libpd_finish_list(destS.c_str());
    return res;
}

void GdPd::print(const std::string& message) {
	UtilityFunctions::print((std::string("GDPD : ")+message).c_str());
}

void GdPd::receiveList(const std::string& dest, const pd::List& list) {
	Array gdlist;

	for(int i = 0; i < list.len(); ++i) {
		if(list.isFloat(i)) {
			gdlist.push_back(list.getFloat(i));
		}
		else if(list.isSymbol(i)) {
			String symbStr(list.getSymbol(i).c_str());
			gdlist.push_back(symbStr);
		}
	}

	m_messages->push_back(gdlist);
}

void GdPd::set_volume(float vol) {
	m_vol=vol;
}

/* GdPdStream */
GdPdStream::GdPdStream() {

}

Ref<AudioStreamPlayback> GdPdStream::_instantiate_playback() const {
    Ref<GdPdStreamPlayback> gdPlayback = memnew(GdPdStreamPlayback());
    //gdPlayback.instantiate();
    gdPlayback->m_base = Ref<GdPdStream>(this);
    return gdPlayback;
}

#define zeromem(to, count) memset(to, 0, count)

/* GdPdStreamplayback */
GdPdStreamPlayback::GdPdStreamPlayback() {
    m_buffer.resize(4096*2,0.0);
    AudioServer::get_singleton()->lock();
	pcm_buffer = memalloc(PCM_BUFFER_SIZE);
	zeromem(pcm_buffer, PCM_BUFFER_SIZE);
	AudioServer::get_singleton()->unlock();
}

GdPdStreamPlayback::~GdPdStreamPlayback() {

}

int32_t GdPdStreamPlayback::_mix_resampled(AudioFrame *buffer, 
                                           int32_t nbFrames) {
    m_buffer.resize(4096*2, 0.0);
    m_base->getGdPd()->processAudio(m_buffer.data(), NULL, nbFrames);
    for(int i = 0; i < nbFrames; i++) {
        buffer[i].left = m_buffer[i*2];
        buffer[i].right = m_buffer[i*2+1];
    }
    return nbFrames;
}

float GdPdStreamPlayback::_get_stream_sampling_rate() const {
    return AudioServer::get_singleton()->get_mix_rate();
}
