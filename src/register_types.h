#ifndef GD4PD_REGISTER_TYPES_H
#define GD4PD_REGISTER_TYPES_H

#include <godot_cpp/core/class_db.hpp>

using namespace godot;

void initialize_gdpd_module(ModuleInitializationLevel p_level);
void uninitialize_gdpd_module(ModuleInitializationLevel p_level);

#endif 
