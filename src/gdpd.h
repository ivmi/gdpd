/***************************************************************************
 *  gdpd.h
 *  Part of GdPd
 *  2023- see README for AUTHORS
 *  https://gitlab.univ-lille.fr/ivmi
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 130, Boston, MA 02111-1307, USA.
 */



#ifndef GDPD_CLASS_H
#define GDPD_CLASS_H

#ifdef WIN32
#include <windows.h>
#endif

#include <godot_cpp/classes/node3d.hpp>
#include <godot_cpp/classes/global_constants.hpp>
#include <godot_cpp/classes/viewport.hpp>
#include <godot_cpp/core/binder_common.hpp>
#include <godot_cpp/classes/engine.hpp>
#include <godot_cpp/classes/audio_stream.hpp>
#include <godot_cpp/classes/audio_stream_playback_resampled.hpp>
#include <godot_cpp/classes/audio_stream_player.hpp>
#include <godot_cpp/classes/audio_frame.hpp>

#include "PdBase.hpp"
#include "RtAudio.h"

using namespace godot;

class GdPdStreamPlayback;


class GdPd : public Node, public pd::PdReceiver {
	GDCLASS(GdPd, Node);

protected:
	static void _bind_methods();

private:
    bool m_realtime;
	float *m_inBuf; 
	float *m_outBuf;
	Array* m_messages;
	pd::PdBase m_pd;
	pd::Patch m_patch;
	RtAudio m_audio;
	unsigned int m_bufferFrames;
	float m_vol;
	int m_nbInputs;
	int m_nbOutputs;
	int m_sampleRate;
	int m_inputDevice;
	int m_outputDevice;
	std::map<std::string, pd::Patch> m_patchsMap;

	bool m_init;

    enum Mode {OSC, AUDIO, AUDIO_RT};
    int m_mode;


private:
	int start();

public:

	GdPd();
	virtual ~GdPd();

	//libpd functions
	Array get_available_input_devices();
	Array get_available_output_devices();
    int init_nort();
	int init_devices(String inputDevice, String outputDevice);
	int init_parameters(int nbInputs, int nbOutputs, int sampleRate, int bufferSize);
	void stop();
	bool openfile(String basename, String dirname);
	void closefile(String basename);
	bool has_message();
	Array get_next();
	int blocksize();
	void subscribe(String symbStr);
	int start_message(int nbValues);
	void add_symbol(String symbStr);
	void add_float(float val);
	int finish_list(String destStr);

	//libpd hooks
	virtual void print(const std::string& message);
	void receiveList(const std::string& dest, const pd::List& list);

	//godot functions
	void set_volume(float vol);
	inline const float& get_volume(){return m_vol;}


	//rtaudio
	static int audioCallback(void *outputBuffer, void *inputBuffer, 
							 unsigned int nBufferFrames, double streamTime, 
							 RtAudioStreamStatus status, void *userData);
	void processAudio(void *outputBuffer, void *inputBuffer, 
					   unsigned int nBufferFrames);
};

class GdPdStream : public AudioStream {
	GDCLASS(GdPdStream, AudioStream);
    friend class GdPdStreamPlayback;

private:
    GdPd* m_gdpd;

protected:
	static void _bind_methods(){}

public:
	GdPdStream();
    Ref<AudioStreamPlayback> _instantiate_playback() const override;

    virtual String _get_stream_name() const {return "GdPd";}
    virtual double _get_length() const { return 0; }

    void setGdPd(GdPd* gdpd){m_gdpd=gdpd;}
    GdPd* getGdPd(){return m_gdpd;}
};


class GdPdStreamPlayback: public AudioStreamPlaybackResampled {
	GDCLASS(GdPdStreamPlayback, AudioStreamPlaybackResampled);
    friend class GdPdStream;

protected:
    Ref<GdPdStream> m_base;
    std::vector<float> m_buffer;
    bool active;
    void *pcm_buffer;

	static void _bind_methods(){}

public:
    GdPdStreamPlayback();
    ~GdPdStreamPlayback();

    int32_t _mix_resampled(AudioFrame *dst_buffer, int32_t frame_count) override;
    float _get_stream_sampling_rate() const override;

    bool _is_playing() const override {return active;}
    void _start(double from_pos) override {active=true;}
    void _seek(double position) override {}
    void _stop() override {active=false;}
};


#endif
